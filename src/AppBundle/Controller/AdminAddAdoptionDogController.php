<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AdminAddAdoptionController
 * @package AppBundle\Controller
 */
class AdminAddAdoptionDogController extends Controller
{
    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function AddAdoptionAction(Request $request)
    {
        $adoption = $this->get('app.pet')->addAdoptionDog($request);

        if ($adoption->isSubmitted() && $adoption->isValid())
        {
            $this->addFlash('notice', 'Adoption enregistré !');
            return $this->redirectToRoute('admin_addAdoptionDog');
        }

        return $this->render('admin/addAdoptionDog.html.twig', [
            'form' => $adoption->createView()
        ]);
    }
}
