<?php
/**
 * Author: Jérôme Brechoire
 * Email: brechoire.j@gmail.com
 */

namespace AppBundle\Controller;


use AppBundle\Entity\Cat;
use AppBundle\Entity\WantedPoster;
use AppBundle\Service\PetService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Class ListWantedController
 * @package AppBundle\Controller
 */
class ListWantedController extends Controller
{
    /**
     * @param $page
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction($page)
    {
        if ($page < 1) {
            throw $this->createNotFoundException("La page ".$page." n'existe pas.");
        }

        $nbPerPage = 12;

        $liste_wanted = $this->getDoctrine()
            ->getManager()
            ->getRepository(WantedPoster::class)
            ->listAllWantedNotFoundValidate($page, $nbPerPage)
        ;

        $nbPages = ceil(count($liste_wanted) / $nbPerPage);

        $count_wanted = $this->get('app.pet')->countAllWantedNotFoundValidate();

        if ($count_wanted != 0)
        {
            if ($page > $nbPages) {
                throw $this->createNotFoundException("La page ".$page." n'existe pas. :(");
            }
        }
        return $this->render('default/liste_recherche.html.twig',
            array(
                'count_wanted' => $count_wanted,
                'liste_wanted' => $liste_wanted,
                'nbPages'     => $nbPages,
                'page'        => $page,
            ));
    }
}