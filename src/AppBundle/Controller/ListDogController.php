<?php
/**
 * Author: Jérôme Brechoire
 * Email: brechoire.j@gmail.com
 */

namespace AppBundle\Controller;


use AppBundle\Entity\Dog;
use AppBundle\Service\PetService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Class ListDogController
 * @package AppBundle\Controller
 */
class ListDogController extends Controller
{
//    /**
//     * @return \Symfony\Component\HttpFoundation\Response
//     */
//    public function listDogAction()
//    {
//        $liste_dog = $this->get('app.pet')->listDog();
//        $count = $this->get('app.pet')->countAllDogNotAdoptedNotDeceased();
//
//        return $this->render('default/liste_chien.html.twig', [
//            'liste_dog' => $liste_dog,
//            'count_dog' => $count
//        ]);
//    }

    public function listDogAction($page)
    {
        if ($page < 1) {
            throw $this->createNotFoundException("La page ".$page." n'existe pas.");
        }

        $nbPerPage = 12;

        $liste_dog = $this->getDoctrine()
            ->getManager()
            ->getRepository(Dog::class)
            ->listDogNotAdoptedNotDeceased($page, $nbPerPage)
        ;

        $nbPages = ceil(count($liste_dog) / $nbPerPage);

        $count_dog = $this->get('app.pet')->countAllDogNotAdoptedNotDeceased();

        if ($count_dog != 0)
        {
            if ($page > $nbPages) {
                throw $this->createNotFoundException("La page ".$page." n'existe pas. :(");
            }
        }
        return $this->render('default/liste_chien.html.twig',
            array(
                'count_dog' => $count_dog,
                'liste_dog' => $liste_dog,
                'nbPages'     => $nbPages,
                'page'        => $page,
            ));
    }
}