<?php
/**
 * Author: Jérôme Brechoire
 * Email: brechoire.j@gmail.com
 */

namespace AppBundle\Controller;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AdminEditDogController
 * @package AppBundle\Controller
 */
class AdminEditDogController extends Controller
{
    /**
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function editDogAction(Request $request, $id)
    {
        $dog = $this->get('app.pet')->findOneDog($id);

        if (!$dog) {
            $this->addFlash('notice', 'Le chien demandé est inconnu');
            return $this->redirectToRoute('homepage');
        }
        $form = $this->get('app.pet')->editDog($request, $id);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->addFlash('notice', 'Modification validé');
            return $this->redirectToRoute('admin_list_dog');
        }

        return $this->render('admin/editDog.html.twig',
            array(
                'dog' => $dog,
                'editDog' => $form->createView()
            ));
    }
}