<?php
/**
 * Author: Jérôme Brechoire
 * Email: brechoire.j@gmail.com
 */

namespace AppBundle\Controller;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AdminEditOwnerController
 * @package AppBundle\Controller
 */
class AdminEditOwnerController extends Controller
{
    /**
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function AdminEditOwnerAction(Request $request, $id)
    {
        $owner = $this->get('app.pet')->findOneOwner($id);

        if (!$owner) {
            $this->addFlash('notice', 'Le propriétaire demandé est inconnu');
            return $this->redirectToRoute('admin_list_owner');
        }
        $form = $this->get('app.pet')->editOwner($request, $id);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->addFlash('notice', 'Modification validé');
            return $this->redirectToRoute('admin_list_owner');
        }

        return $this->render('admin/editOwner.html.twig',
            array(
                'owner' => $owner,
                'editOwner' => $form->createView()
            ));
    }
}