<?php

namespace AppBundle\Controller;

use AppBundle\Entity\WantedPoster;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Class AdminIndexController
 * @package AppBundle\Controller
 */
class AdminIndexController extends Controller
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function AdminIndexAction()
    {
    	$countCatAdoptedNotDeceased = $this->get('app.petstats')->countCatAdoptedNotDeceased();
    	$countDogAdoptedNotDeceased = $this->get('app.petstats')->countDogAdoptedNotDeceased();
    	$countCatNotAdoptedNotDeceased = $this->get('app.petstats')->countCatNotAdoptedNotDeceased();
    	$countDogNotAdoptedNotDeceased = $this->get('app.petstats')->countDogNotAdoptedNotDeceased();

    	$month = $this->getDoctrine()->getRepository(WantedPoster::class)->threeLastMonth();

        return $this->render('admin/index.html.twig', [
        	'countCatAdoptedNotDeceased' => $countCatAdoptedNotDeceased,
        	'countDogAdoptedNotDeceased' => $countDogAdoptedNotDeceased,
            'countCatNotAdoptedNotDeceased' => $countCatNotAdoptedNotDeceased,
            'countDogNotAdoptedNotDeceased' => $countDogNotAdoptedNotDeceased,
            'month' => $month
        	]);
    }

}
