<?php
/**
 * Author: Jérôme Brechoire
 * Email: brechoire.j@gmail.com
 */

namespace AppBundle\Controller;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AdminAddOwnerController
 * @package AppBundle\Controller
 */
class AdminAddOwnerController extends Controller
{
    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function addOwnerAction(Request $request)
    {
        $addOwner = $this->get('app.pet')->addOwner($request);

        if ($addOwner->isSubmitted() && $addOwner->isValid())
        {
            $this->addFlash('notice', 'Propriétaire ajouté');
            return $this->redirectToRoute('admin_list_owner');
        }

        return $this->render('admin/addOwner.html.twig',[
            'addOwner' => $addOwner->createView()
        ]);
    }
}