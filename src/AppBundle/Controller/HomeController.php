<?php
/**
 * Author: Jérôme Brechoire
 * Email: brechoire.j@gmail.com
 */

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Class HomeController
 * @package AppBundle\Controller
 */
class HomeController extends Controller
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function homeAction()
    {
        $countCatNotAdoptedNotDeceased = $this->get('app.petstats')->countCatNotAdoptedNotDeceased();
        $countDogNotAdoptedNotDeceased = $this->get('app.petstats')->countDogNotAdoptedNotDeceased();
        //$this->get('session')->set('avis', $countAvis);
        return $this->render('default/index.html.twig',[
            'countCatNotAdoptedNotDeceased' => $countCatNotAdoptedNotDeceased,
            'countDogNotAdoptedNotDeceased' => $countDogNotAdoptedNotDeceased
        ]);
    }
}