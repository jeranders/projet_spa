<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Class AdminListDogController
 * @package AppBundle\Controller
 */
class AdminListDogController extends Controller
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function AdminListDogAction()
    {
        $liste_dog = $this->get('app.pet')->listDogAdmin();
        $count = $this->get('app.pet')->countAllDog();

        return $this->render('admin/listDog.html.twig', [
            'liste_dog' => $liste_dog,
            'count_dog' => $count
        ]);
    }
}
