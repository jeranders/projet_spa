<?php
/**
 * Author: Jérôme Brechoire
 * Email: brechoire.j@gmail.com
 */

namespace AppBundle\Controller;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AdminEditCatController
 * @package AppBundle\Controller
 */
class AdminEditCatController extends Controller
{
    /**
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function editCatAction(Request $request, $id)
    {
        $cat = $this->get('app.pet')->findOneCat($id);

        if (!$cat) {
            $this->addFlash('notice', 'Le chat demandé est inconnu');
            return $this->redirectToRoute('admin_list_cat');
        }
        $form = $this->get('app.pet')->editCat($request, $id);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->addFlash('notice', 'Modification validé');
            return $this->redirectToRoute('admin_list_cat');
        }

        return $this->render('admin/editCat.html.twig',
            array(
                'cat' => $cat,
                'editCat' => $form->createView()
            ));
    }
}