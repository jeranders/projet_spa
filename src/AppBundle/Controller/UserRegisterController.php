<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class UserRegisterController extends Controller
{
    public function UserRegisterAction(Request $request)
    {
        if ($this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_REMEMBERED'))
        {
            return $this->redirectToRoute('homepage');
        }

        $count = $this->get('app.user')->countUser();

        if ($count != 0)
        {
            return $this->redirectToRoute('homepage');
        }

        $form = $this->get('app.user')->registerUser($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $this->addFlash('notice','Inscription validé !');
            return $this->redirectToRoute('homepage');
        }

        return $this->render(
            'default/inscription.html.twig',
            array('form' => $form->createView())
        );
    }
}
