<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class AdminValidationWantedYesController extends Controller
{
    /**
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function indexAction($id)
    {
        $warn = $this->get('app.pet');
        $warn->ValidationWantedYes($id);

        $this->addFlash('notice', 'Validation effectué');

        return $this->redirectToRoute('admin_validation_wanted');
    }
}
