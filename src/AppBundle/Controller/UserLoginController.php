<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class UserLoginController extends Controller
{
    public function UserLoginAction()
    {
        if ($this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_REMEMBERED'))
        {
            return $this->redirectToRoute('homepage');
        }

        $login = $this->get('app.user')->loginUser();

        $counUser = $this->get('app.user')->countUser();

        return $this->render(
            'default/connexion.html.twig',
            array(
                'login' => $login,
                'countUser' => $counUser
            ));
    }
}
