<?php
/**
 * Author: Jérôme Brechoire
 * Email: brechoire.j@gmail.com
 */

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Ajout d'un chien
 *
 * Class AddCatController
 * @package AppBundle\Controller
 */
class AdminAddCatController extends Controller
{
    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function addCatAction(Request $request)
    {
        $addCat = $this->get('app.pet')->addCat($request);

        if ($addCat->isSubmitted() && $addCat->isValid())
        {
            $this->addFlash('notice', 'Chat bien ajouté dans la base de données');
            return $this->redirectToRoute('admin_addCat');
        }

        return $this->render('admin/addCat.html.twig', [
            'addCat' => $addCat->createView()
        ]);
    }
}