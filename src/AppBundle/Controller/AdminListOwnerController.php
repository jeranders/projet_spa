<?php
namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Class AdminListOwnerController
 * @package AppBundle\Controller
 */
class AdminListOwnerController extends Controller
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function AdminListOwnerAction()
    {
        $liste_owner = $this->get('app.pet')->listOwner();
        $count = $this->get('app.pet')->countAllOwner();
        
        return $this->render('admin/listOwner.html.twig', [
            'liste_owner' => $liste_owner,
            'count_owner' => $count
        ]);
    }
}

