<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Class AdminListCatController
 * @package AppBundle\Controller
 */
class AdminListCatController extends Controller
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function AdminListCatAction()
    {
        $liste_cat = $this->get('app.pet')->listCatAdmin();
        $count = $this->get('app.pet')->countAllCat();

        return $this->render('admin/listCat.html.twig', [
            'liste_cat' => $liste_cat,
            'count_cat' => $count
        ]);
    }
}
