<?php
/**
 * Author: Jérôme Brechoire
 * Email: brechoire.j@gmail.com
 */

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Ajout d'un chien
 *
 * Class AddDogController
 * @package AppBundle\Controller
 */
class AdminAddDogController extends Controller
{
    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function addDogAction(Request $request)
    {
        $addDog = $this->get('app.pet')->addDog($request);

        if ($addDog->isSubmitted() && $addDog->isValid())
        {
            $this->addFlash('notice', 'Chien bien ajouté dans la base de données');
            return $this->redirectToRoute('admin_addDog');
        }

        return $this->render('admin/addDog.html.twig', [
            'addDog' => $addDog->createView()
        ]);
    }
}