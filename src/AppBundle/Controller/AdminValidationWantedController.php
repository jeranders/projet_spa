<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Class AdminValidationWantedController
 * @package AppBundle\Controller
 */
class AdminValidationWantedController extends Controller
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function indexAction()
    {
        $liste = $this->get('app.pet')->WantedListById();

        return $this->render('admin/validationWanted.html.twig', [
            'liste_wanted' => $liste
        ]);
    }
}
