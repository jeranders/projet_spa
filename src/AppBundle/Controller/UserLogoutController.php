<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class UserLogoutController extends Controller
{
    public function logoutAction()
    {
        throw new \Exception('this should not be reached!');
    }
}
