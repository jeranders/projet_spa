<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Class AdminValidationFoundYesController
 * @package AppBundle\Controller
 */
class AdminValidationFoundYesController extends Controller
{
    /**
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function indexAction($id)
    {
        $warn = $this->get('app.pet');
        $warn->ValidationFoundYes($id);

        $this->addFlash('notice', 'Animal retrouvé !');

        return $this->redirectToRoute('admin_validation_wanted');
    }
}
