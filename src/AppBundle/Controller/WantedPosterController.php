<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class WantedPosterController extends Controller
{
    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function WantedPosterAction(Request $request)
    {
        $wanted = $this->get('app.pet')->addWanted($request);

        if ($wanted->isSubmitted() && $wanted->isValid()){
            $this->addFlash('notice', 'Votre avis de recherche à bien était envoyé. Il sera validé dans les plus brefs délais.');
            return $this->redirectToRoute('wanted');
        }

        return $this->render('default/avis_de_recherche.html.twig', [
            'form' => $wanted->createView()
        ]);
    }
}
