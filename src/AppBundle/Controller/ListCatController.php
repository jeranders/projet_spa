<?php
/**
 * Author: Jérôme Brechoire
 * Email: brechoire.j@gmail.com
 */

namespace AppBundle\Controller;


use AppBundle\Entity\Cat;
use AppBundle\Service\PetService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Class ListCatController
 * @package AppBundle\Controller
 */
class ListCatController extends Controller
{
//    /**
//     * @return \Symfony\Component\HttpFoundation\Response
//     */
//    public function listCatAction()
//    {
//        $liste_cat = $this->get('app.pet')->listCat();
//        $count = $this->get('app.pet')->countAllCatNotAdoptedNotDeceased();
//
//        return $this->render('default/liste_chat.html.twig', [
//            'liste_cat' => $liste_cat,
//            'count_cat' => $count
//        ]);
//    }

    /**
     * @param $page
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listCatAction($page)
    {
        if ($page < 1) {
            throw $this->createNotFoundException("La page ".$page." n'existe pas.");
        }

        $nbPerPage = 2;

        $liste_cat = $this->getDoctrine()
            ->getManager()
            ->getRepository(Cat::class)
            ->listCatNotAdoptedNotDeceased($page, $nbPerPage)
        ;

        $nbPages = ceil(count($liste_cat) / $nbPerPage);

        $count_cat = $this->get('app.pet')->countAllCatNotAdoptedNotDeceased();

        if ($count_cat != 0)
        {
            if ($page > $nbPages) {
                throw $this->createNotFoundException("La page ".$page." n'existe pas. :(");
            }
        }
        return $this->render('default/liste_chat.html.twig',
            array(
                'count_cat' => $count_cat,
                'liste_cat' => $liste_cat,
                'nbPages'     => $nbPages,
                'page'        => $page,
            ));
    }
}