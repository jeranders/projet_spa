<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Class AdminValidationFoundNoController
 * @package AppBundle\Controller
 */
class AdminValidationFoundNoController extends Controller
{
    /**
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function indexAction($id)
    {
        $warn = $this->get('app.pet');
        $warn->ValidationFoundNo($id);

        $this->addFlash('notice', 'Animal perdu :(');

        return $this->redirectToRoute('admin_validation_wanted');
    }
}
