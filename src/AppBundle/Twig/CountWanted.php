<?php
/**
 * Author: Jérôme Brechoire
 * Email: brechoire.j@gmail.com
 */

namespace AppBundle\Twig;


use AppBundle\Entity\WantedPoster;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class CountWanted
 * @package AppBundle\Twig
 */
class CountWanted extends \Twig_Extension
{

    private $doctrine;

    /**
     * CountWanted constructor.
     * @param EntityManagerInterface $doctrine
     */
    public function __construct(EntityManagerInterface $doctrine)
    {
        $this->doctrine = $doctrine;
    }

    /**
     * @return array
     */
    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('CountWanted', array($this, 'CountWantedPet')),
        );
    }

    /**
     * @return mixed
     */
    public function CountWantedPet()
    {
        $count = $this->doctrine->getRepository(WantedPoster::class)->countAllWantedNotFoundNotValidate();

        return $count;
    }
}