<?php
/**
 * Author: Jérôme Brechoire
 * Email: brechoire.j@gmail.com
 */

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Class AdoptionCatRepository
 * @package AppBundle\Repository
 */
class AdoptionCatRepository extends EntityRepository
{

}