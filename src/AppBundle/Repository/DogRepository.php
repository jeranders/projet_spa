<?php
/**
 * Author: Jérôme Brechoire
 * Email: brechoire.j@gmail.com
 */

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;

/**
 * Class DogRepository
 * @package AppBundle\Repository
 */
class DogRepository extends EntityRepository
{
    /**
     * Compte tout les chiens de la BDD
     *
     * @return mixed
     */
    public function countAllDog()
    {
        return $this->createQueryBuilder('d')
            ->select('count(d.id)')
            ->where('d.adoption = 0')
            ->andWhere('d.deceased = 0')
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * Affiche les chiens qui ne sont pas mort ou adopter
     *
     * @return array
     */
    public function adoptionYes()
    {
        return $this->createQueryBuilder('d')
            ->select('d.id')
            ->where('d.adoption = 0')
            ->andWhere('d.deceased = 0')
            ->getQuery()
            ->getResult();
    }

    /**
     * Compte les chiens adopter non décédé
     * @return mixed
     */
    public function countDogAdoptedNotDeceased()
    {
        return $this->createQueryBuilder('d')
            ->select('count(d.id)')
            ->where('d.adoption = 1')
            ->andWhere('d.deceased = 0')
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * Compte le nombre de chiens non adopté non décédé
     * @return mixed
     */
    public function countAllDogNotAdoptedNotDeceased()
    {
        return $this->createQueryBuilder('d')
            ->select('count(d.id)')
            ->where('d.adoption = 0')
            ->andWhere('d.deceased = 0')
            ->getQuery()
            ->getSingleScalarResult();
    }

//    /**
//     * Liste des chiens non adopté non décédé
//     * @return array
//     */
//    public function listDogNotAdoptedNotDeceased()
//    {
//        return $this->createQueryBuilder('d')
//            ->leftJoin('d.image', 'i')
//            ->addSelect('i')
//            ->where('d.adoption = 0')
//            ->andWhere('d.deceased = 0')
//            ->getQuery()
//            ->getResult();
//    }

    /**
     * @param $page
     * @param $nbPerPage
     * @return Paginator
     */
    public function listDogNotAdoptedNotDeceased($page, $nbPerPage)
    {
        $query = $this->createQueryBuilder('d')
            ->leftJoin('d.image', 'i')
            ->addSelect('i')
            ->where('d.adoption = 0')
            ->andWhere('d.deceased = 0')
            ->orderBy('d.dateArriving', 'DESC')
            ->getQuery()
        ;
        $query
            ->setFirstResult(($page-1) * $nbPerPage)
            ->setMaxResults($nbPerPage)
        ;
        return new Paginator($query, true);
    }

    /**
     * Compte les chiens qui ne sont ni adopté ni décédé
     * @return mixed
     */
    public function countDogNotAdoptedNotDeceased()
    {
        return $this->createQueryBuilder('d')
            ->select('count(d.id)')
            ->where('d.adoption = 0')
            ->andWhere('d.deceased = 0')
            ->getQuery()
            ->getSingleScalarResult();
    }
}
