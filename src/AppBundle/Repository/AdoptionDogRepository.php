<?php
/**
 * Author: Jérôme Brechoire
 * Email: brechoire.j@gmail.com
 */

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Class AdoptionDogRepository
 * @package AppBundle\Repository
 */
class AdoptionDogRepository extends EntityRepository
{

}