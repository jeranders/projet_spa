<?php
/**
 * Author: Jérôme Brechoire
 * Email: brechoire.j@gmail.com
 */

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;

/**
 * Class CatRepository
 * @package AppBundle\Repository
 */
class CatRepository extends EntityRepository
{
    /**
     * Compte tous les chats
     * @return mixed
     */
    public function countAllCat()
    {
        return $this->createQueryBuilder('c')
            ->select('count(c.id)')
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function countAllCatNotAdoptedNotDeceased()
    {
        return $this->createQueryBuilder('c')
            ->select('count(c.id)')
            ->where('c.adoption = 0')
            ->andWhere('c.deceased = 0')
            ->getQuery()
            ->getSingleScalarResult();
    }

//    /**
//     * Liste des chats non adopté non décédé
//     * @return array
//     */
//    public function listCatNotAdoptedNotDeceased()
//    {
//        return $this->createQueryBuilder('c')
//            ->leftJoin('c.image', 'i')
//            ->addSelect('i')
//            ->where('c.adoption = 0')
//            ->andWhere('c.deceased = 0')
//            ->getQuery()
//            ->getResult();
//    }

    /**
     * @param $page
     * @param $nbPerPage
     * @return Paginator
     */
    public function listCatNotAdoptedNotDeceased($page, $nbPerPage)
    {
        $query = $this->createQueryBuilder('c')
            ->leftJoin('c.image', 'i')
            ->addSelect('i')
            ->where('c.adoption = 0')
            ->andWhere('c.deceased = 0')
            ->orderBy('c.dateArriving', 'DESC')
            ->getQuery()
        ;
        $query
            ->setFirstResult(($page-1) * $nbPerPage)
            ->setMaxResults($nbPerPage)
        ;
        return new Paginator($query, true);
    }

    /**
     * Compte les chats adopter non décédé
     * @return mixed
     */
    public function countCatAdoptedNotDeceased()
    {
        return $this->createQueryBuilder('c')
            ->select('count(c.id)')
            ->where('c.adoption = 1')
            ->andWhere('c.deceased = 0')
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * Compte les chats qui ne sont ni adopté ni décédé
     * @return mixed
     */
    public function countCatNotAdoptedNotDeceased()
    {
        return $this->createQueryBuilder('c')
            ->select('count(c.id)')
            ->where('c.adoption = 0')
            ->andWhere('c.deceased = 0')
            ->getQuery()
            ->getSingleScalarResult();
    }
}