<?php
/**
 * Author: Jérôme Brechoire
 * Email: brechoire.j@gmail.com
 */

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Class OwnerRepository
 * @package AppBundle\Repository
 */
class OwnerRepository extends EntityRepository
{
    /**
     * Compte tout les chiens de la BDD
     *
     * @return mixed
     */
    public function countAllOwner()
    {
        return $this->createQueryBuilder('o')
        ->select('count(o.id)')
        ->getQuery()
        ->getSingleScalarResult();
    }
}