<?php

/**
 * Author: Jérôme Brechoire
 * Email: brechoire.j@gmail.com
 */

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;

/**
 * Class WantedPosterRepository
 * @package AppBundle\Repository
 */
class WantedPosterRepository extends EntityRepository
{
    /**
     * Retourne les animaux disparu non validé non trouvé
     * @return mixed
     */
    public function WantedNoValid()
    {
        return $this->createQueryBuilder('w')
            ->select('count(w.id)')
            ->where('w.valide = 0')
            ->andWhere('w.found = 0')
            ->getQuery()
            ->getSingleResult();
    }

    /**
     * @return array
     */
    public function WantedListById()
    {
        return $this->createQueryBuilder('w')
            ->leftJoin('w.image', 'i')
            ->addSelect('i')
            ->orderBy('w.id', 'ASC')
            ->getQuery()
            ->getResult();
    }

    /**
     * @return mixed
     */
    public function countAllWantedNotFoundValidate()
    {
        return $this->createQueryBuilder('w')
            ->select('count(w.id)')
            ->where('w.found = 0')
            ->andWhere('w.valide = 1')
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * @return mixed
     */
    public function countAllWantedNotFoundNotValidate()
    {
        return $this->createQueryBuilder('w')
            ->select('count(w.id)')
            ->where('w.found = 0')
            ->andWhere('w.valide = 0')
            ->getQuery()
            ->getSingleScalarResult();
    }
    /**
     * @param $page
     * @param $nbPerPage
     * @return Paginator
     */
    public function listAllWantedNotFoundValidate($page, $nbPerPage)
    {
        $query = $this->createQueryBuilder('w')
            ->leftJoin('w.image', 'i')
            ->addSelect('i')
            ->where('w.found = 0')
            ->andWhere('w.valide = 1')
            ->orderBy('w.registrationDate', 'DESC')
            ->getQuery()
        ;
        $query
            ->setFirstResult(($page-1) * $nbPerPage)
            ->setMaxResults($nbPerPage)
        ;
        return new Paginator($query, true);
    }

    public function threeLastMonth()
    {
        $date = new \DateTime();
        $date->modify('-1 month');
        return $this->createQueryBuilder('w')
            ->andWhere('w.dateDisappearance <= :date')
            ->setParameter(':date', $date)
            ->getQuery()
            ->getResult()
            ;
    }
}