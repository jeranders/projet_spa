<?php
/**
 * Author: Jérôme Brechoire
 * Email: brechoire.j@gmail.com
 */

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Class ImageDogRepository
 * @package AppBundle\Repository
 */
class ImageDogRepository extends EntityRepository
{

}