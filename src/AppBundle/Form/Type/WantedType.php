<?php

namespace AppBundle\Form\Type;

use AppBundle\Entity\WantedPoster;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;

class WantedType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'Nom de l\'animal perdu *',
                'required' => true,
                'constraints' => [
                    new NotBlank([
                        'message' => 'Le nom ne doit pas être vide'
                    ]),
                    new Length([
                        'min' => 3,
                        'minMessage' => 'Le nom doit avoir 3 caractères minimum',
                        'max' => 40,
                        'maxMessage' => 'Le nom doit avoir 40 caractères minimum'
                    ])
                ]
            ])
            ->add('phone', TextType::class, [
                'label' => 'Numéro de téléphone *',
                'required' => true,
                'constraints' => [
                    new NotBlank([
                        'message' => 'Le numéro de téléphone ne peut pas être vide'
                    ]),
                    new Length([
                        'min' => 10,
                        'max' => 10,
                        'exactMessage' => 'Le numéro de téléphone doit faire exactement 10 caractères'
                    ])
                ]
            ])
            ->add('tattooNumber', TextType::class, [
                'label' => 'Numéro de tatouage',
                'required' => false,
                'constraints' => [
                    new Length([
                        'max' => 20,
                        'maxMessage' => 'Le numéro de tatouage doit avoir 20 caractères maximum'
                    ])
                ]
            ])
            ->add('dateDisappearance', DateTimeType::class, [
                'label' => 'Date de disparition *',
                'widget' => 'single_text',
                //'format' => 'yyyy-mm-dd',
                'attr' => [
                    'class' => 'datepicker'
                ],
                'constraints' => [
                    new NotBlank([
                        'message' => 'Le champ ne doit pas être vide'
                    ])
                ]
            ])
            ->add('comment', TextareaType::class, [
                'label' => 'Commentaire *',

                'required' => false,
                'attr' => [
                    'class' => 'materialize-textarea',
                    'placeholder' => 'Exemple : Il porte un collier, a peur des chats...',
                ],
                'constraints' => [
                    new Length([
                        'max' => 225,
                        'maxMessage' => 'Le commentaire doit avoir 225 caractères maximum'
                    ]),
                    new NotBlank([
                        'message' => 'Le commentaire est obligatoire'
                    ])
                ]
            ])
            ->add('city', TextType::class, [
                'label' => 'Ville/Commune *',
                'required' => true,
                'constraints' => [
                    new Length([
                        'max' => 3,
                        'maxMessage' => 'Le nom de la ville/Commune doit faire 3 caractères maximum',
                        'max' => 20,
                        'maxMessage' => 'Le nom de la ville/Commune doit faire 20 caractères maximum'
                    ]),
                    new NotBlank([
                        'message' => 'Le nom de la ville est obligatoire'
                    ])
                ]
            ])
            ->add('image', ImageWantedType::class, [
                'required' => true,
                'label' => false,
                'attr' => [
                    'class' => 'file-path validate'
                ]
            ])
            ->add('type', ChoiceType::class, [
                'label' => 'Animal',
                'choices' => [
                    'Chien' => true,
                    'Chat' => false
                ]
            ])
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => WantedPoster::class
        ]);
    }

    public function getBlockPrefix()
    {
        return 'app_bundle_wanted_type';
    }
}
