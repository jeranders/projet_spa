<?php
/**
 * Author: Jérôme Brechoire
 * Email: brechoire.j@gmail.com
 */

namespace AppBundle\Form\Type;


use AppBundle\Entity\Dog;
use AppBundle\Entity\ImageDog;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;

/**
 * Class DogType
 * @package AppBundle\Form\Type
 */
class DogType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'Nom du chien *',
                'required' => true,
                'attr' => [
                    'class' => 'form-control col-md-7 col-xs-12'
                ],
                'constraints' => [
                    new NotBlank([
                        'message' => 'Le champ ne doit pas être vide'
                    ]),
                   new Length([
                        'min' => 3,
                        'minMessage' => 'Le titre doit avoir 3 caractères minimum',
                        'max' => 255,
                        'maxMessage' => 'Le titre doit avoir 255 caractères maximum'
                    ])
                ]
            ])
            ->add('sexe', ChoiceType::class, [
                'label' => 'Sexe du chien',
                'choices' => [
                    'Male' => true,
                    'Femelle' => false
                ],
                'attr' => [
                    'class' => 'form-control col-md-7 col-xs-12'
                ],
                'constraints' => [
                ]
            ])
            ->add('puce', TextType::class, [
                'label' => 'Numéro d\'identification de la puce',
                'required' => false,
                'attr' => [
                    'class' => 'form-control col-md-7 col-xs-12'
                ]
            ])
            ->add('castrated', ChoiceType::class, [
                'choices' => [
                    'Oui' => true,
                    'Non' => false
                ],
                'label' => 'Est-il castré ?',
                'attr' => [
                    'class' => 'form-control col-md-7 col-xs-12'
                ]
            ])
            ->add('races', EntityType::class, [
                'label' => 'Race *',
                'class' => 'AppBundle\Entity\RaceDog',
                'choice_label' => 'name',
                'multiple' => true,
                'attr' => [
                    'class' => 'race form-control col-md-7 col-xs-12',
                    'multiple' => 'multiple'
                ]
            ])
            ->add('vaccine', ChoiceType::class, [
                'choices' => [
                    'Oui' => true,
                    'Non' => false
                ],
                'label' => 'Est-il vacciné ?',
                'attr' => [
                    'class' => 'form-control col-md-7 col-xs-12'
                ]
            ])
            ->add('dog', ChoiceType::class, [
                'choices' => [
                    'Oui' => 2,
                    'Non' => 1,
                    'Iconnu' => 0
                ],
                'label' => 'S\'entend t-il avec les chiens ?',
                'attr' => [
                    'class' => 'form-control col-md-7 col-xs-12'
                ]
            ])
            ->add('cat', ChoiceType::class, [
                'choices' => [
                    'Oui' => 2,
                    'Non' => 1,
                    'Iconnu' => 0
                ],
                'label' => 'S\'entend t-il avec les chats ?',
                'attr' => [
                    'class' => 'form-control col-md-7 col-xs-12'
                ]
            ])
            ->add('children', ChoiceType::class, [
                'choices' => [
                    'Oui' => 2,
                    'Non' => 1,
                    'Iconnu' => 0
                ],
                'label' => 'S\'entend t-il avec les enfants',
                'attr' => [
                    'class' => 'form-control col-md-7 col-xs-12'
                ]
            ])
            ->add('dateBirthday', DateTimeType::class, [
                'label' => 'Date de naissance du chien',
                'widget' => 'single_text',
                'format' => 'dd/MM/yyyy',
                'attr' => [
                    'class' => 'date-calendar form-control col-md-7 col-xs-12'
                ],
                'constraints' => [
                    new NotBlank([
                        'message' => 'Le champ ne doit pas être vide'
                    ])
                ]
            ])
            ->add('dateArriving', DateTimeType::class, [
                'label' => 'Date d\'arrivé dans le refuge du chien ?',
                'widget' => 'single_text',
                'format' => 'dd/MM/yyyy',
                'attr' => [
                    'class' => 'date-calendar form-control col-md-7 col-xs-12'
                ],
                'constraints' => [
                    new NotBlank([
                        'message' => 'Le champ ne doit pas être vide'
                    ])
                ]
            ])
            ->add('size', ChoiceType::class, [
                'choices' => [
                    'Grand' => 3,
                    'Moyen' => 2,
                    'Petit' => 1
                ],
                'label' => 'Quel est la taille',
                'attr' => [
                    'class' => 'form-control col-md-7 col-xs-12'
                ]
            ])
            ->add('deceased', ChoiceType::class, [
                'choices' => [
                    'Non' => false,
                    'Oui' => true
                ],
                'label' => 'Est-il décédé ?',
                'attr' => [
                    'class' => 'form-control col-md-7 col-xs-12'
                ]
            ])
            ->add('comment', TextareaType::class,[
                'label' => 'Commentaire',
                'attr' => [
                    'class' => 'form-control col-md-7 col-xs-12'
                ]
            ])
            ->add('image', ImageDogType::class, [
                'required' => false,
                'label' => 'Photo du chien'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Dog::class,
        ]);
    }
}