<?php
/**
 * Author: Jérôme Brechoire
 * Email: brechoire.j@gmail.com
 */

namespace AppBundle\Form\Type;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Validator\Constraints as Assert;

class ImageCatType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('file', FileType::class, array(
                'label' => false,
                'attr' => array(
                    'accept' => 'image/*',
                    'capture' => '',
                ),
                'constraints' => new Assert\Image(),
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\ImageCat'
        ));
    }

    public function getBlockPrefix()
    {
        return 'app_bundle_image_cat_type';
    }
}