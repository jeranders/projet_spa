<?php

namespace AppBundle\Form\Type;

use AppBundle\Entity\AdoptionCat;
use AppBundle\Repository\CatRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class AdoptionCatType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('dateAdoption', DateTimeType::class, [
                'label' => 'Date d\'adoption',
                'widget' => 'single_text',
                'format' => 'dd-MM-yyyy',
                'attr' => [
                    'class' => 'date-calendar form-control col-md-7 col-xs-12'
                ],
                 'constraints' => [
                    new NotBlank([
                        'message' => 'Aucun date de selectionné'
                        ])
                ]
            ])
            ->add('owner', EntityType::class, [
                'label' => 'Propriétaire',
                'class' => 'AppBundle\Entity\Owner',
                'required' => true,
                'choice_label' => 'fullName',
                'attr' => [
                    'class' => 'select-adoption form-control col-md-7 col-xs-12',
                ],
                'constraints' => [
                    new NotBlank([
                        'message' => 'Aucun propriétaire de selectionné'
                        ])
                ]
            ])
            ->add('cat', EntityType::class, [
                'label' => 'Chat',
                'class' => 'AppBundle\Entity\Cat',
                'choice_label' => 'name',
                'required' => true,
                'attr' => [
                    'class' => 'select-adoption form-control col-md-7 col-xs-12',
                ],
                'query_builder' => function (CatRepository $er) {
                    return $er->createQueryBuilder('c')
                        ->where('c.adoption = 0')
                        ->andWhere('c.deceased = 0');
                },
                 'constraints' => [
                    new NotBlank([
                        'message' => 'Aucun chat de selectionné'
                        ])
                ]
            ])
            ->add('adoptionCosts', CheckboxType::class, [
                'label'    => 'Frais d\'adoption payé ?',
                'required' => false
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
           'data_class' => AdoptionCat::class
        ]);
    }

    public function getBlockPrefix()
    {
        return 'app_bundle_adoption_cat_type';
    }
}
