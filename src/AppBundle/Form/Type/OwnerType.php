<?php
/**
 * Author: Jérôme Brechoire
 * Email: brechoire.j@gmail.com
 */

namespace AppBundle\Form\Type;


use AppBundle\Entity\Owner;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;

class OwnerType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName', TextType::class,[
                'label' => 'Prénom *',
                'attr' => [
                    'class' => 'form-control col-md-7 col-xs-12'
                ],
                'required' => true,
                'constraints'=> [
                    new Length([
                        'min' => 3,
                        'minMessage' => 'Le prénom doit avoir 3 caractères minimum',
                        'max' => 20,
                        'maxMessage' => 'Le prénom doit avoir 20 caractères maximum'
                    ]),
                    new NotBlank(),
                    new NotNull()
                ]
            ])
            ->add('lastName', TextType::class,[
                'label' => 'Nom *',
                'attr' => [
                    'class' => 'form-control col-md-7 col-xs-12'
                ],
                'required' => true,
                'constraints'=> [
                    new Length([
                        'min' => 3,
                        'minMessage' => 'Le nom doit avoir 3 caractères minimum',
                        'max' => 20,
                        'maxMessage' => 'Le nom doit avoir 20 caractères maximum'
                    ]),
                    new NotBlank(),
                    new NotNull()
                ]
            ])
            ->add('email', TextType::class,[
                'label' => 'Email *',
                'attr' => [
                    'class' => 'form-control col-md-7 col-xs-12'
                ],
                'required' => false,
                'constraints'=> [
                    new Email(),
                    new NotBlank(),
                    new NotNull()
                ]
            ])
            ->add('phone', TextType::class,[
                'label' => 'Numéro de téléphone *',
                'attr' => [
                    'class' => 'form-control col-md-7 col-xs-12'
                ],
                'required' => true,
                'constraints'=> [
                    new Length([
                        'min' => 10,
                        'max' => 10,
                        'exactMessage' => 'Le numéro de téléphone doit avoir 10 caractères'
                    ]),
                    new NotBlank(),
                    new NotNull()
                ]
            ])
            ->add('address', TextType::class,[
                'label' => 'Adresse *',
                'attr' => [
                    'class' => 'form-control col-md-7 col-xs-12'
                ],
                'required' => true,
                'constraints'=> [
                    new NotBlank(),
                    new NotNull()
                ]
            ])
            ->add('postalCode', TextType::class,[
                'label' => 'Code postal *',
                'attr' => [
                    'class' => 'form-control col-md-7 col-xs-12'
                ],
                'required' => true,
                'constraints'=> [
                    new NotBlank(),
                    new NotNull()
                ]
            ])
            ->add('city', TextType::class,[
                'label' => 'Ville *',
                'attr' => [
                    'class' => 'form-control col-md-7 col-xs-12'
                ],
                'required' => true,
                'constraints'=> [
                    new NotBlank(),
                    new NotNull()
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Owner::class,
        ]);
    }
}