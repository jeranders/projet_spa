<?php

namespace AppBundle\Form\Type;

use AppBundle\Entity\AdoptionDog;
use AppBundle\Repository\DogRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AdoptionDogType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('dateAdoption', DateTimeType::class, [
                'label' => 'Date d\'adoption',
                'widget' => 'single_text',
                'attr' => [
                    'class' => 'date-calendar form-control col-md-7 col-xs-12'
                ]
            ])
            ->add('owner', EntityType::class, [
                'label' => 'Propriétaire',
                'class' => 'AppBundle\Entity\Owner',
                'choice_label' => 'fullName',
                'attr' => [
                    'class' => 'select-adoption form-control col-md-7 col-xs-12',
                ]
            ])
            ->add('dog', EntityType::class, [
                'label' => 'Chien',
                'class' => 'AppBundle\Entity\Dog',
                'choice_label' => 'name',
                'attr' => [
                    'class' => 'select-adoption form-control col-md-7 col-xs-12',
                ],
                'query_builder' => function (DogRepository $er) {
                    return $er->createQueryBuilder('d')
                        ->where('d.adoption = 0')
                        ->andWhere('d.deceased = 0');
                },
            ])
            ->add('adoptionCosts', CheckboxType::class, [
                'label'    => 'Frais d\'adoption payé ?',
                'required' => false
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
           'data_class' => AdoptionDog::class
        ]);
    }

    public function getBlockPrefix()
    {
        return 'app_bundle_adoption_dog_type';
    }
}
