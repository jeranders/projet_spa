<?php
/**
 * Author: Jérôme Brechoire
 * Email: brechoire.j@gmail.com
 */

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Cat
 *
 * @ORM\Table(name="cat")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CatRepository")
 */
class Cat
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_birthday", type="datetime", nullable=false)
     */
    private $dateBirthday;

    /**
     * @var bool
     *
     * @ORM\Column(name="sexe", type="boolean")
     */
    private $sexe;

    /**
     * @var string
     *
     * @ORM\Column(name="puce", type="string", length=255, nullable=true)
     */
    private $puce;

    /**
     * @var bool
     *
     * @ORM\Column(name="castrated", type="boolean")
     */
    private $castrated;

    /**
     * @var bool
     *
     * @ORM\Column(name="vaccine", type="boolean")
     */
    private $vaccine;

    /**
     * @var string
     *
     * @ORM\Column(name="dog", type="string")
     */
    private $dog;

    /**
     * @var string
     *
     * @ORM\Column(name="cat", type="string")
     */
    private $cat;

    /**
     * @var string
     *
     * @ORM\Column(name="children", type="string")
     */
    private $children;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", nullable=true)
     */
    private $comment;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_arriving", type="datetime", nullable=false)
     */
    private $dateArriving;

    /**
     * @var RaceCat[]|ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\RaceCat", cascade={"persist"})
     */
    private $races;

    /**
     * @var int
     *
     * @ORM\Column(name="size", type="integer")
     */
    private $size;

    /**
     * @var boolean
     *
     * @ORM\Column(name="adoption", type="boolean")
     */
    private $adoption;

    /**
     * @var boolean
     *
     * @ORM\Column(name="deceased", type="boolean")
     */
    private $deceased;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\ImageCat", cascade={"persist", "remove"})
     */
    private $image;

    /**
     * Dog constructor.
     */
    public function __construct()
    {
        $this->races = new ArrayCollection();
        $this->adoption = 0;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return \DateTime
     */
    public function getDateBirthday()
    {
        return $this->dateBirthday;
    }

    /**
     * @param \DateTime $dateBirthday
     */
    public function setDateBirthday($dateBirthday)
    {
        $this->dateBirthday = $dateBirthday;
    }

    /**
     * @return bool
     */
    public function isSexe()
    {
        return $this->sexe;
    }

    /**
     * @param bool $sexe
     */
    public function setSexe($sexe)
    {
        $this->sexe = $sexe;
    }

    /**
     * @return string
     */
    public function getPuce()
    {
        return $this->puce;
    }

    /**
     * @param string $puce
     */
    public function setPuce($puce)
    {
        $this->puce = $puce;
    }

    /**
     * @return bool
     */
    public function isCastrated()
    {
        return $this->castrated;
    }

    /**
     * @param bool $castrated
     */
    public function setCastrated($castrated)
    {
        $this->castrated = $castrated;
    }

    /**
     * @return bool
     */
    public function isVaccine()
    {
        return $this->vaccine;
    }

    /**
     * @param bool $vaccine
     */
    public function setVaccine($vaccine)
    {
        $this->vaccine = $vaccine;
    }

    /**
     * @return string
     */
    public function getDog()
    {
        return $this->dog;
    }

    /**
     * @param string $dog
     */
    public function setDog($dog)
    {
        $this->dog = $dog;
    }

    /**
     * @return string
     */
    public function getCat()
    {
        return $this->cat;
    }

    /**
     * @param string $cat
     */
    public function setCat($cat)
    {
        $this->cat = $cat;
    }

    /**
     * @return string
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @param string $children
     */
    public function setChildren($children)
    {
        $this->children = $children;
    }

    /**
     * @return \DateTime
     */
    public function getDateArriving()
    {
        return $this->dateArriving;
    }

    /**
     * @param \DateTime $dateArriving
     */
    public function setDateArriving($dateArriving)
    {
        $this->dateArriving = $dateArriving;
    }

    /**
     * @return int
     */
    public function getAge()
    {
        $dateInterval = $this->dateBirthday->diff(new \DateTime());

        return $dateInterval->y;
    }

    /**
     * @return int
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * @param int $size
     */
    public function setSize($size)
    {
        $this->size = $size;
    }

    /**
     * @param mixed $races
     */
    public function setRaces($races)
    {
        $this->races = $races;
    }

    /**
     * @param RaceCat $raceCat
     */
    public function addRace(RaceCat $raceCat)
    {
        if (!$this->races->contains($raceCat)) {
            $this->races->add($raceCat);
        }
    }

    /**
     * @param RaceCat $raceCat
     */
    public function removeRace(RaceCat $raceCat)
    {
        $this->races->removeElement($raceCat);
    }

    /**
     * @return RaceCat[]|ArrayCollection
     */
    public function getRaces()
    {
        return $this->races;
    }

    /**
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param string $comment
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    }

    /**
     * @return bool
     */
    public function isAdoption()
    {
        return $this->adoption;
    }

    /**
     * @param bool $adoption
     */
    public function setAdoption($adoption)
    {
        $this->adoption = $adoption;
    }

    /**
     * @return bool
     */
    public function isDeceased()
    {
        return $this->deceased;
    }

    /**
     * @param bool $deceased
     */
    public function setDeceased($deceased)
    {
        $this->deceased = $deceased;

    }

    /**
     * @return mixed
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param mixed $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }


}

