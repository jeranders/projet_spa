<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class WantedPoster
 * @package AppBundle\Entity
 *
 * @ORM\Table(name="wanted_poster")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\WantedPosterRepository")
 */
class WantedPoster
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string")
     */
    private $name;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\ImageWanted", cascade={"persist", "remove"})
     */
    private $image;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string")
     */
    private $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="tattoo_number", type="string", nullable=true)
     */
    private $tattooNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text")
     */
    private $comment;

    /**
     * @var \DateTime
     * @ORM\Column(name="registration_date", type="datetime")
     */
    private $registrationDate;

    /**
     * @var \DateTime
     * @ORM\Column(name="date_disappearance", type="datetime")
     */
    private $dateDisappearance;

    /**
     * @var string
     * @ORM\Column(name="city", type="string")
     */
    private $city;

    /**
     * @var boolean
     * @ORM\Column(name="type", type="boolean")
     */
    private $type;

    /**
     * @var boolean
     * @ORM\Column(name="valide", type="boolean")
     */
    private $valide;

    /**
     * @var boolean
     * @ORM\Column(name="found", type="boolean")
     */
    private $found;

    /**
     * WantedPoster constructor.
     */
    public function __construct()
    {
        $this->registrationDate = new \DateTime();
        $this->valide = 0;
        $this->found = 0;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param mixed $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return string
     */
    public function getTattooNumber()
    {
        return $this->tattooNumber;
    }

    /**
     * @param string $tattooNumber
     */
    public function setTattooNumber($tattooNumber)
    {
        $this->tattooNumber = $tattooNumber;
    }

    /**
     * @return mixed
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param mixed $comment
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    }

    /**
     * @return \DateTime
     */
    public function getRegistrationDate()
    {
        return $this->registrationDate;
    }

    /**
     * @param \DateTime $registrationDate
     */
    public function setRegistrationDate($registrationDate)
    {
        $this->registrationDate = $registrationDate;
    }

    /**
     * @return \DateTime
     */
    public function getDateDisappearance()
    {
        return $this->dateDisappearance;
    }

    /**
     * @param \DateTime $dateDisappearance
     */
    public function setDateDisappearance($dateDisappearance)
    {
        $this->dateDisappearance = $dateDisappearance;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param string $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @return bool
     */
    public function isType()
    {
        return $this->type;
    }

    /**
     * @param bool $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return bool
     */
    public function isValide()
    {
        return $this->valide;
    }

    /**
     * @param bool $valide
     */
    public function setValide($valide)
    {
        $this->valide = $valide;
    }

    /**
     * @return bool
     */
    public function isFound()
    {
        return $this->found;
    }

    /**
     * @param bool $found
     */
    public function setFound($found)
    {
        $this->found = $found;
    }



}