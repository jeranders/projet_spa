<?php
/**
 * Author: Jérôme Brechoire
 * Email: brechoire.j@gmail.com
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class AdoptionCat
 * @package AppBundle\Entity
 *
 * @ORM\Table(name="adoption_cat")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AdoptionCatRepository")
 */
class AdoptionCat
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var \DateTime
     * @ORM\Column(name="date_adoption", type="datetime")
     */
    private $dateAdoption;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Owner")
     * @ORM\JoinColumn(nullable=false)
     */
    private $owner;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Cat")
     * @ORM\JoinColumn(nullable=false, referencedColumnName="id")
     */
    private $cat;

    /**
     * @var boolean
     *
     * @ORM\Column(name="adoption_costs", type="boolean")
     */
    private $adoptionCosts;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return \DateTime
     */
    public function getDateAdoption()
    {
        return $this->dateAdoption;
    }

    /**
     * @param \DateTime $dateAdoption
     */
    public function setDateAdoption($dateAdoption)
    {
        $this->dateAdoption = $dateAdoption;
    }

    /**
     * @return mixed
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * @param mixed $owner
     */
    public function setOwner($owner)
    {
        $this->owner = $owner;
    }

    /**
     * @return int
     */
    public function getCat()
    {
        return $this->cat;
    }

    /**
     * @param int $cat
     */
    public function setCat($cat)
    {
        $this->cat = $cat;
    }

    /**
     * @return bool
     */
    public function isAdoptionCosts()
    {
        return $this->adoptionCosts;
    }

    /**
     * @param bool $adoptionCosts
     */
    public function setAdoptionCosts($adoptionCosts)
    {
        $this->adoptionCosts = $adoptionCosts;
    }

}