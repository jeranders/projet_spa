<?php
/**
 * Author: Jérôme Brechoire
 * Email: brechoire.j@gmail.com
 */

namespace AppBundle\DataFixtures\ORM;


use AppBundle\Entity\RaceDog;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadRace implements FixtureInterface
{

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        // Liste des noms à ajouter
        $names = array(
            'Race 1',
            'Race 2',
            'Race 3',
            'Race 4',
            'Race 5'
        );

        foreach ($names as $name) {
            // On crée la race
            $race = new RaceDog();
            $race->setName($name);

            // On la persiste
            $manager->persist($race);
        }

        // On déclenche l'enregistrement de toutes les races
        $manager->flush();
    }
}