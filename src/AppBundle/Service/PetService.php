<?php
/**
 * Author: Jérôme Brechoire
 * Email: brechoire.j@gmail.com
 */

namespace AppBundle\Service;

use AppBundle\Entity\AdoptionCat;
use AppBundle\Entity\AdoptionDog;
use AppBundle\Entity\Cat;
use AppBundle\Entity\Dog;
use AppBundle\Entity\Owner;
use AppBundle\Entity\WantedPoster;
use AppBundle\Form\Type\AdoptionCatType;
use AppBundle\Form\Type\AdoptionDogType;
use AppBundle\Form\Type\CatType;
use AppBundle\Form\Type\DogType;
use AppBundle\Form\Type\OwnerType;
use AppBundle\Form\Type\WantedType;
use AppBundle\Repository\WantedPosterRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class PetService
 * @package AppBundle\Service
 */
class PetService
{
    /**
     * @var EntityManagerInterface
     */
    private $doctrine;

    /**
     * @var FormFactoryInterface
     */
    private $form;

    /**
     * @var FileUploader
     */
    private $fileuploader;

    /**
     * PetService constructor.
     * @param EntityManagerInterface $doctrine
     * @param FormFactoryInterface $form
     * @param FileUploader $fileuploader
     */
    public function __construct(EntityManagerInterface $doctrine, FormFactoryInterface $form, FileUploader $fileuploader)
    {
        $this->doctrine     = $doctrine;
        $this->form         = $form;
        $this->fileuploader = $fileuploader;
    }

    /**
     * Ajout d'un chien via formulaire
     *
     * @param Request $request
     * @return \Symfony\Component\Form\FormInterface
     */
    public function addDog(Request $request)
    {
        $dog = new Dog();
        $form = $this->form->create(DogType::class, $dog);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {

            $form->getData();
            $listRace = $form['races']->getData();
            foreach ($listRace as $race)
            {
                $dog->addRace($race);
            }

            if($form['image']->getData() != null){
                $file = $form['image']->getData()->getFile();
                $fileName = $this->fileuploader->upload($file);
                $dog->getImage()->setAlt($fileName);
            }

            $this->doctrine->persist($dog);
            $this->doctrine->flush();
        }

        return $form;
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\Form\FormInterface
     */
    public function addWanted(Request $request)
    {
        $wanted = new WantedPoster();
        $form = $this->form->create(WantedType::class, $wanted);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {

            $form->getData();
          
               $file = $form['image']->getData()->getFile();
               $fileName = $this->fileuploader->upload($file);
               $wanted->getImage()->setAlt($fileName);
            

            $this->doctrine->persist($wanted);
            $this->doctrine->flush();
        }

        return $form;
    }

    /**
     * Ajout d'un chat via formulaire
     *
     * @param Request $request
     * @return \Symfony\Component\Form\FormInterface
     */
    public function addCat(Request $request)
    {
        $cat = new Cat();
        $form = $this->form->create(CatType::class, $cat);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {

            $form->getData();
            $listRace = $form['races']->getData();
            foreach ($listRace as $race)
            {
                $cat->addRace($race);
            }

            if($form['image']->getData() != null){
                $file = $form['image']->getData()->getFile();
                $fileName = $this->fileuploader->upload($file);
                $cat->getImage()->setAlt($fileName);
            }

            $this->doctrine->persist($cat);
            $this->doctrine->flush();
        }

        return $form;
    }

    /**
     * Edition d'un chien
     *
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\Form\FormInterface
     */
    public function editDog(Request $request, $id)
    {
        $dog = $this->doctrine
            ->getRepository(Dog::class)
            ->findOneBy(array('id' => $id));

        $form = $this->form->create(DogType::class, $dog);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {

            $form->getData();
            $listRace = $form['races']->getData();
            foreach ($listRace as $race)
            {
                $dog->addRace($race);
            }

            if($form['image']->getData() != null){
                $file = $form['image']->getData()->getFile();
                $fileName = $this->fileuploader->upload($file);
                $dog->getImage()->setAlt($fileName);
            }

            $this->doctrine->flush();
        }

        return $form;
    }

    /**
     * Edition d'un chat
     *
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\Form\FormInterface
     */
    public function editCat(Request $request, $id)
    {
        $cat = $this->doctrine
            ->getRepository(Cat::class)
            ->findOneBy(array('id' => $id));

        $form = $this->form->create(CatType::class, $cat);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {

            $form->getData();
            $listRace = $form['races']->getData();
            foreach ($listRace as $race)
            {
                $cat->addRace($race);
            }

            if($form['image']->getData() != null){
                $file = $form['image']->getData()->getFile();
                $fileName = $this->fileuploader->upload($file);
                $cat->getImage()->setAlt($fileName);
            }


            $this->doctrine->flush();
        }

        return $form;
    }

    /**
     * Edition d'un chat
     *
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\Form\FormInterface
     */
    public function editOwner(Request $request, $id)
    {
        $cat = $this->doctrine
            ->getRepository(Owner::class)
            ->findOneBy(array('id' => $id));

        $form = $this->form->create(OwnerType::class, $cat);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $this->doctrine->flush();
        }

        return $form;
    }

    /**
     * Liste des chats
     *
     * @return Dog[]|array
     */
    public function listDog()
    {
        $liste_dog = $this->doctrine->getRepository(Dog::class)->listDogNotAdoptedNotDeceased();

        return $liste_dog;
    }

    /**
     * Liste des chiens ADMIN
     *
     * @return Dog[]|array
     */
    public function listDogAdmin()
    {
        $liste_dog = $this->doctrine->getRepository(Dog::class)->findAll();

        return $liste_dog;
    }

    /**
     * Liste des chats
     *
     * @return Cat[]|array
     */
    public function listCat()
    {
        $liste_cat = $this->doctrine->getRepository(Cat::class)->listCatNotAdoptedNotDeceased();

        return $liste_cat;
    }

    /**
     * Liste des chats ADMIN
     *
     * @return Cat[]|array
     */
    public function listCatAdmin()
    {
        $liste_cat = $this->doctrine->getRepository(Cat::class)->findAll();

        return $liste_cat;
    }

    /**
     *  Liste des propri�taires
     *
     * @return Owner[]|array
     */
    public function listOwner()
    {
        $liste_owner = $this->doctrine->getRepository(Owner::class)->findAll();

        return $liste_owner;
    }

    /**
     * Recherche chien par son ID
     *
     * @param $id
     * @return Dog|null|object
     */
    public function findOneDog($id)
    {
        $dog = $this->doctrine->getRepository(Dog::class)->find($id);
        return $dog;
    }

    /**
     * Recherche propri�taire par son ID
     *
     * @param $id
     * @return Owner|null|object
     */
    public function findOneOwner($id)
    {
        $owner = $this->doctrine->getRepository(Owner::class)->find($id);
        return $owner;
    }

    /**
     * Recherche chat par son ID
     *
     * @param $id
     * @return Cat|null|object
     */
    public function findOneCat($id)
    {
        $cat = $this->doctrine->getRepository(Cat::class)->find($id);
        return $cat;
    }

    /**
     * Compte tous les chiens
     *
     * @return mixed
     */
    public function countAllDog()
    {
        $count = $this->doctrine->getRepository(Dog::class)->countAllDog();

        return $count;
    }

    /**
     * Compte tous les chats
     *
     * @return mixed
     */
    public function countAllCat()
    {
        $count = $this->doctrine->getRepository(Cat::class)->countAllCat();

        return $count;
    }

    /**
     * Compte tous les chats non adopté non décédé
     *
     * @return mixed
     */
    public function countAllCatNotAdoptedNotDeceased()
    {
        $count = $this->doctrine->getRepository(Cat::class)->countAllCatNotAdoptedNotDeceased();

        return $count;
    }

    /**
     * @return mixed
     */
    public function countAllWantedNotFoundValidate()
    {
        $count = $this->doctrine->getRepository(WantedPoster::class)->countAllWantedNotFoundValidate();

        return $count;
    }

    /**
     * Compte tous les chiens non adopté non décédé
     *
     * @return mixed
     */
    public function countAllDogNotAdoptedNotDeceased()
    {
        $count = $this->doctrine->getRepository(Dog::class)->countDogNotAdoptedNotDeceased();

        return $count;
    }

    /**
     * Compte tous les propri�taires
     *
     * @return mixed
     */
    public function countAllOwner()
    {
        $count = $this->doctrine->getRepository(Owner::class)->countAllOwner();

        return $count;
    }

    /**
     * Ajout d'un propriétaire
     *
     * @param Request $request
     * @return \Symfony\Component\Form\FormInterface
     */
    public function addOwner(Request $request)
    {
        $owner = new Owner();
        $form = $this->form->create(OwnerType::class, $owner);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            $this->doctrine->persist($owner);
            $this->doctrine->flush();
        }

        return $form;
    }

    /**
     * Ajout d'une adoption chien
     *
     * @param Request $request
     * @return \Symfony\Component\Form\FormInterface
     */
    public function addAdoptionDog(Request $request)
    {
        $adoption = new AdoptionDog();
        $form = $this->form->create(AdoptionDogType::class, $adoption);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $this->doctrine->persist($adoption);
            $this->doctrine->flush();

            $dog = $this->doctrine->getRepository(Dog::class)->find($adoption->getDog());
            $dog->setAdoption(1);
            $this->doctrine->flush();
        }

        return $form;

    }

    /**
     * Ajout d'une adoption chat
     *
     * @param Request $request
     * @return \Symfony\Component\Form\FormInterface
     */
    public function addAdoptionCat(Request $request)
    {
        $adoption = new AdoptionCat();
        $form = $this->form->create(AdoptionCatType::class, $adoption);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $this->doctrine->persist($adoption);
            $this->doctrine->flush();

            $dog = $this->doctrine->getRepository(Cat::class)->find($adoption->getCat());
            $dog->setAdoption(1);
            $this->doctrine->flush();
        }

        return $form;

    }

    /**
     * @return mixed
     */
    public function WantedNoValide()
    {
        $count = $this->doctrine->getRepository(WantedPoster::class)->WantedNoValid();

        return $count;
    }

    public function WantedListById()
    {
        $list = $this->doctrine->getRepository(WantedPoster::class)->WantedListById();

        return $list;
    }

    /**
     * @param $id
     * @return WantedPoster|null|object
     */
    public function ValidationWantedYes($id)
    {
        $val = $this->doctrine
            ->getRepository(WantedPoster::class)
            ->findOneBy(array('id' => $id));

        $val->setValide(1);
        $this->doctrine->flush();

        return $val;
    }

    /**
     * @param $id
     * @return WantedPoster|null|object
     */
    public function ValidationWantedNo($id)
    {
        $val = $this->doctrine
            ->getRepository(WantedPoster::class)
            ->findOneBy(array('id' => $id));

        $val->setValide(0);
        $this->doctrine->flush();

        return $val;
    }

    /**
     * @param $id
     * @return WantedPoster|null|object
     */
    public function ValidationFoundYes($id)
    {
        $val = $this->doctrine
            ->getRepository(WantedPoster::class)
            ->findOneBy(array('id' => $id));

        $val->setFound(1);
        $val->setValide(0);
        $this->doctrine->flush();

        return $val;
    }

    /**
     * @param $id
     * @return WantedPoster|null|object
     */
    public function ValidationFoundNo($id)
    {
        $val = $this->doctrine
            ->getRepository(WantedPoster::class)
            ->findOneBy(array('id' => $id));

        $val->setFound(0);
        $this->doctrine->flush();

        return $val;
    }
}