<?php
/**
 * Author: Jérôme Brechoire
 * Email: brechoire.j@gmail.com
 */

namespace AppBundle\Service;


use AppBundle\Entity\Cat;
use AppBundle\Entity\Dog;
use Doctrine\ORM\EntityManagerInterface;

class PetStatsService
{
    /**
     * @var EntityManagerInterface
     */
    private $doctrine;

    /**
     * PetStatsService constructor.
     * @param EntityManagerInterface $doctrine
     */
    public function __construct(EntityManagerInterface $doctrine)
    {
        $this->doctrine = $doctrine;
    }

    /**
     * Compte les chats adopté non décédé
     * @return mixed
     */
    public function countCatAdoptedNotDeceased()
    {
        $count = $this->doctrine->getRepository(Cat::class)->countCatAdoptedNotDeceased();

        return $count;
    }

    /**
     * Compte les chats non adopté non décédé
     * @return mixed
     */
    public function countCatNotAdoptedNotDeceased()
    {
        $count = $this->doctrine->getRepository(Cat::class)->countCatNotAdoptedNotDeceased();

        return $count;
    }

    /**
     * Compte les chiens adopté non décédé
     * @return mixed
     */
    public function countDogAdoptedNotDeceased()
    {
        $count = $this->doctrine->getRepository(Dog::class)->countDogAdoptedNotDeceased();

        return $count;
    }

    /**
     * Compte les chiens non adopté non décédé
     * @return mixed
     */
    public function countDogNotAdoptedNotDeceased()
    {
        $count = $this->doctrine->getRepository(Dog::class)->countDogNotAdoptedNotDeceased();

        return $count;
    }
}