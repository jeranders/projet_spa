<?php
/**
 * Author: Jérôme Brechoire
 * Email: brechoire.j@gmail.com
 */

namespace AppBundle\Service;

use AppBundle\Entity\User;
use AppBundle\Form\Type\ModifPasswordType;
use AppBundle\Form\Type\RegisterType;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoder;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Csrf\TokenStorage\TokenStorageInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;

class UserService
{
    /**
     * @var EntityManagerInterface
     */
    private $doctrine;

    /**
     * @var FormFactoryInterface
     */
    private $form;

    /**
     * @var UserPasswordEncoderInterface
     */
    private $passEncoder;

    /**
     * @var AuthenticationUtils
     */
    private $loginUtils;

    /**
     * @var AuthorizationCheckerInterface
     */
    private $secuCheck;

    /**
     * @var TokenStorageInterface
     */
    private $security;

    /**
     * UserService constructor.
     * @param EntityManagerInterface $doctrine
     * @param FormFactoryInterface $form
     * @param UserPasswordEncoderInterface $passEncoder
     * @param AuthenticationUtils $loginUtils
     * @param AuthorizationCheckerInterface $secuCheck
     * @param TokenStorage $security
     */
    public function __construct(EntityManagerInterface $doctrine, FormFactoryInterface $form, UserPasswordEncoderInterface $passEncoder, AuthenticationUtils $loginUtils, AuthorizationCheckerInterface $secuCheck, TokenStorage $security)
    {
        $this->doctrine = $doctrine;
        $this->form = $form;
        $this->passEncoder = $passEncoder;
        $this->loginUtils = $loginUtils;
        $this->secuCheck = $secuCheck;
        $this->security = $security;
    }

    /**
     * Formulaire d'inscription d'un utilisateur
     * User registration form
     *
     * @param Request $request
     * @return \Symfony\Component\Form\FormInterface
     */
    public function registerUser(Request $request)
    {
        $user = new User();

        $form = $this->form->create(RegisterType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            // Role par défault
            $user->setRoles('ROLE_ADMIN');

            $password = $this->passEncoder->encodePassword(
                $user,
                $user->getPassword()
            );

            $user->setPassword($password);

            $this->doctrine->persist($user);
            $this->doctrine->flush();
        }

        return $form;

    }

    /**
     * Formulaire de modification de mot de passe
     *
     * @param Request $request
     * @return \Symfony\Component\Form\FormInterface
     */
    public function postModifPass(Request $request)
    {
        $user = $this->getUser();
        $form = $this->form->create(ModifPasswordType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $password = $this->passEncoder->encodePassword(
                $user,
                $user->getPlainPassword()
            );
            $user->setPassword($password);

            $this->doctrine->persist($user);
            $this->doctrine->flush();

        }
        return $form;
    }

    /**
     * Formulaire de connexion
     * Connection form
     * @return array
     */
    public function loginUser()
    {
        return array(
            '_username' => $this->loginUtils->getLastUsername(),
            'error' => $this->loginUtils->getLastAuthenticationError()
        );
    }

    public function countUser()
    {
        $count = $this->doctrine->getRepository(User::class)->countUser();
        return $count;
    }
}