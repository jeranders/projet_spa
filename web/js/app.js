$( document ).ready(function() {

    /**
     * Affiche le champ d'ajout du code de la puce de l'animal
     */
    $('#puce-choice').change(function () {

        var val = $('#puce-choice option:selected').val();

        if (val == 1)
        {
            $('.puce-choice-yes').show();
        }else{
            $('.puce-choice-yes').hide();
        }

    });

    $('select').material_select();

});

$('.datepicker').pickadate({
    weekdaysLetter: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
    monthsFull: [ 'Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre' ],
    monthsShort: [ 'Jan', 'Fev', 'Mar', 'Avr', 'Mai', 'Juin', 'Juil', 'Aou', 'Sep', 'Oct', 'Nov', 'Dec' ],
    weekdaysFull: [ 'Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi' ],
    weekdaysShort: [ 'Dim', 'Lun', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam' ],
    today: 'Aujourd\'hui',
    clear: 'Effacer',
    close: 'Fermer',
    firstDay: 1,
    format: 'dd mmmm yyyy',
    formatSubmit: 'yyyy/mm/dd',
    labelMonthNext:"Mois suivant",
    labelMonthPrev:"Mois précédent",
    labelMonthSelect:"Sélectionner un mois",
    labelYearSelect:"Sélectionner une année",
    selectMonths: true,
    selectYears: 30
});


$('.race').select2({
    maximumSelectionLength: 2
});
