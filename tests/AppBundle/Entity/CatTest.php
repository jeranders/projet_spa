<?php
/**
 * Author: Jérôme Brechoire
 * Email: brechoire.j@gmail.com
 */

namespace Tests\AppBundle\Entity;


use AppBundle\Entity\Cat;
use PHPUnit\Framework\TestCase;

class CatTest extends TestCase
{
    public function testCat(){

        $cat = new Cat();

        $cat->setName('Polo');
        $cat->setRaces(['Race 1', 'Race 2']);
        $cat->setDateBirthday('21/01/2017');
        $cat->setDateArriving('22/01/2017');
        $cat->setDog(1);
        $cat->setSexe(1);
        $cat->setAdoption(0);

        $this->assertEquals('Polo', $cat->getName());
        $this->assertEquals(['Race 1', 'Race 2'], $cat->getRaces());
        $this->assertEquals('21/01/2017', $cat->getDateBirthday());
        $this->assertEquals('22/01/2017', $cat->getDateArriving());
        $this->assertEquals(1, $cat->getDog());
        $this->assertEquals(1, $cat->isSexe());
        $this->assertEquals(0, $cat->isAdoption());
    }
}