<?php
/**
 * Author: Jérôme Brechoire
 * Email: brechoire.j@gmail.com
 */

namespace Tests\AppBundle\Entity;


use AppBundle\Entity\Owner;
use PHPUnit\Framework\TestCase;

class OwnerTest extends TestCase
{
    public function testOwner()
    {
        $owner = new Owner();

        $owner->setFirstName('Jérôme');
        $owner->setLastName('Brechoire');
        $owner->setEmail('brechoire.j@gmail.com');
        $owner->setAddress('8B Quartier de la Galoperie');
        $owner->setPostalCode('59186');
        $owner->setCity('Anor');
        $owner->setPhone('0102030405');

        $this->assertEquals('Brechoire', $owner->getLastName());
        $this->assertEquals('Jérôme', $owner->getFirstName());
        $this->assertEquals('brechoire.j@gmail.com', $owner->getEmail());
        $this->assertEquals('8B Quartier de la Galoperie', $owner->getAddress());
        $this->assertEquals('59186', $owner->getPostalCode());
        $this->assertEquals('Anor', $owner->getCity());
        $this->assertEquals('0102030405', $owner->getPhone());
    }
}