<?php
/**
 * Author: Jérôme Brechoire
 * Email: brechoire.j@gmail.com
 */

namespace Tests\AppBundle\Controller;


use AppBundle\Entity\Owner;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class AdminEditOwnerControllerTest extends WebTestCase
{
    /**
     * Test edition d'un propriétaire
     */
    public function testEditOwner()
    {
        $newOwnerEmail = 'toto@toto.fr';

        $client = static::createClient([], [
            'PHP_AUTH_USER' => 'jeranders',
            'PHP_AUTH_PW' => 'jerome',
        ]);

        $crawler = $client->request('GET', 'admin/edit_proprietaire/1');

        $form = $crawler->selectButton('Envoyer')->form([
            'post[email]' => $newOwnerEmail,
        ]);

        $client->submit($form);
        $this->assertSame(Response::HTTP_FOUND, $client->getResponse()->getStatusCode());

        $post = $client->getContainer()->get('doctrine')->getRepository(Owner::class)->find(1);
        $this->assertSame($newOwnerEmail, $post->getEmail());
    }

}